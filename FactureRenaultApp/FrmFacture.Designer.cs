﻿namespace FactureRenaultApp
{
    partial class FrmFacture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDateFacture = new System.Windows.Forms.Label();
            this.dtpDateFacture = new System.Windows.Forms.DateTimePicker();
            this.lblNumOrdre = new System.Windows.Forms.Label();
            this.numOrdre = new System.Windows.Forms.NumericUpDown();
            this.numActivite = new System.Windows.Forms.NumericUpDown();
            this.lblNumAct = new System.Windows.Forms.Label();
            this.grpFacture = new System.Windows.Forms.GroupBox();
            this.lblId = new System.Windows.Forms.Label();
            this.numId = new System.Windows.Forms.NumericUpDown();
            this.grpClient = new System.Windows.Forms.GroupBox();
            this.txtMobile = new System.Windows.Forms.TextBox();
            this.lblMobile = new System.Windows.Forms.Label();
            this.txtTelephone = new System.Windows.Forms.TextBox();
            this.lblTelephone = new System.Windows.Forms.Label();
            this.txtPrenom = new System.Windows.Forms.TextBox();
            this.lblPrenom = new System.Windows.Forms.Label();
            this.txtNom = new System.Windows.Forms.TextBox();
            this.lblNom = new System.Windows.Forms.Label();
            this.grpVehicule = new System.Windows.Forms.GroupBox();
            this.txtGamme = new System.Windows.Forms.TextBox();
            this.lblGamme = new System.Windows.Forms.Label();
            this.lblKms = new System.Windows.Forms.Label();
            this.numKms = new System.Windows.Forms.NumericUpDown();
            this.txtImmat = new System.Windows.Forms.TextBox();
            this.lblImmat = new System.Windows.Forms.Label();
            this.txtSerie = new System.Windows.Forms.TextBox();
            this.lblSerie = new System.Windows.Forms.Label();
            this.btnExport = new System.Windows.Forms.Button();
            this.gridMO = new System.Windows.Forms.DataGridView();
            this.gridPiece = new System.Windows.Forms.DataGridView();
            this.lblTotalMo = new System.Windows.Forms.Label();
            this.numTotalMo = new System.Windows.Forms.NumericUpDown();
            this.lblTotalMoEuros = new System.Windows.Forms.Label();
            this.lblTotalPieceEuros = new System.Windows.Forms.Label();
            this.numTotalPiece = new System.Windows.Forms.NumericUpDown();
            this.lblTotalPiece = new System.Windows.Forms.Label();
            this.gridMontantTva = new System.Windows.Forms.DataGridView();
            this.gridTotalTva = new System.Windows.Forms.DataGridView();
            this.lblTotalAPayerEuros = new System.Windows.Forms.Label();
            this.numTotalAPayer = new System.Windows.Forms.NumericUpDown();
            this.lblTotalAPayer = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numOrdre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numActivite)).BeginInit();
            this.grpFacture.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numId)).BeginInit();
            this.grpClient.SuspendLayout();
            this.grpVehicule.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numKms)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPiece)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTotalMo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTotalPiece)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMontantTva)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTotalTva)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTotalAPayer)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDateFacture
            // 
            this.lblDateFacture.AutoSize = true;
            this.lblDateFacture.Location = new System.Drawing.Point(6, 48);
            this.lblDateFacture.Name = "lblDateFacture";
            this.lblDateFacture.Size = new System.Drawing.Size(69, 13);
            this.lblDateFacture.TabIndex = 3;
            this.lblDateFacture.Text = "Date Facture";
            // 
            // dtpDateFacture
            // 
            this.dtpDateFacture.Location = new System.Drawing.Point(90, 45);
            this.dtpDateFacture.Name = "dtpDateFacture";
            this.dtpDateFacture.Size = new System.Drawing.Size(200, 20);
            this.dtpDateFacture.TabIndex = 4;
            // 
            // lblNumOrdre
            // 
            this.lblNumOrdre.AutoSize = true;
            this.lblNumOrdre.Location = new System.Drawing.Point(6, 73);
            this.lblNumOrdre.Name = "lblNumOrdre";
            this.lblNumOrdre.Size = new System.Drawing.Size(58, 13);
            this.lblNumOrdre.TabIndex = 5;
            this.lblNumOrdre.Text = "Num Ordre";
            // 
            // numOrdre
            // 
            this.numOrdre.Location = new System.Drawing.Point(90, 71);
            this.numOrdre.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numOrdre.Name = "numOrdre";
            this.numOrdre.Size = new System.Drawing.Size(120, 20);
            this.numOrdre.TabIndex = 6;
            // 
            // numActivite
            // 
            this.numActivite.Location = new System.Drawing.Point(90, 97);
            this.numActivite.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numActivite.Name = "numActivite";
            this.numActivite.Size = new System.Drawing.Size(120, 20);
            this.numActivite.TabIndex = 8;
            // 
            // lblNumAct
            // 
            this.lblNumAct.AutoSize = true;
            this.lblNumAct.Location = new System.Drawing.Point(6, 99);
            this.lblNumAct.Name = "lblNumAct";
            this.lblNumAct.Size = new System.Drawing.Size(67, 13);
            this.lblNumAct.TabIndex = 7;
            this.lblNumAct.Text = "Num Activité";
            // 
            // grpFacture
            // 
            this.grpFacture.Controls.Add(this.lblId);
            this.grpFacture.Controls.Add(this.numId);
            this.grpFacture.Controls.Add(this.lblDateFacture);
            this.grpFacture.Controls.Add(this.dtpDateFacture);
            this.grpFacture.Controls.Add(this.lblNumOrdre);
            this.grpFacture.Controls.Add(this.numOrdre);
            this.grpFacture.Controls.Add(this.lblNumAct);
            this.grpFacture.Controls.Add(this.numActivite);
            this.grpFacture.Enabled = false;
            this.grpFacture.Location = new System.Drawing.Point(12, 12);
            this.grpFacture.Name = "grpFacture";
            this.grpFacture.Size = new System.Drawing.Size(315, 130);
            this.grpFacture.TabIndex = 0;
            this.grpFacture.TabStop = false;
            this.grpFacture.Text = "Facture";
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(6, 22);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(44, 13);
            this.lblId.TabIndex = 1;
            this.lblId.Text = "Numéro";
            // 
            // numId
            // 
            this.numId.Location = new System.Drawing.Point(90, 19);
            this.numId.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.numId.Name = "numId";
            this.numId.Size = new System.Drawing.Size(100, 20);
            this.numId.TabIndex = 2;
            // 
            // grpClient
            // 
            this.grpClient.Controls.Add(this.txtMobile);
            this.grpClient.Controls.Add(this.lblMobile);
            this.grpClient.Controls.Add(this.txtTelephone);
            this.grpClient.Controls.Add(this.lblTelephone);
            this.grpClient.Controls.Add(this.txtPrenom);
            this.grpClient.Controls.Add(this.lblPrenom);
            this.grpClient.Controls.Add(this.txtNom);
            this.grpClient.Controls.Add(this.lblNom);
            this.grpClient.Enabled = false;
            this.grpClient.Location = new System.Drawing.Point(333, 12);
            this.grpClient.Name = "grpClient";
            this.grpClient.Size = new System.Drawing.Size(206, 130);
            this.grpClient.TabIndex = 13;
            this.grpClient.TabStop = false;
            this.grpClient.Text = "Client";
            // 
            // txtMobile
            // 
            this.txtMobile.Location = new System.Drawing.Point(100, 97);
            this.txtMobile.Name = "txtMobile";
            this.txtMobile.Size = new System.Drawing.Size(100, 20);
            this.txtMobile.TabIndex = 27;
            // 
            // lblMobile
            // 
            this.lblMobile.AutoSize = true;
            this.lblMobile.Location = new System.Drawing.Point(10, 100);
            this.lblMobile.Name = "lblMobile";
            this.lblMobile.Size = new System.Drawing.Size(38, 13);
            this.lblMobile.TabIndex = 26;
            this.lblMobile.Text = "Mobile";
            // 
            // txtTelephone
            // 
            this.txtTelephone.Location = new System.Drawing.Point(100, 71);
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Size = new System.Drawing.Size(100, 20);
            this.txtTelephone.TabIndex = 25;
            // 
            // lblTelephone
            // 
            this.lblTelephone.AutoSize = true;
            this.lblTelephone.Location = new System.Drawing.Point(10, 74);
            this.lblTelephone.Name = "lblTelephone";
            this.lblTelephone.Size = new System.Drawing.Size(58, 13);
            this.lblTelephone.TabIndex = 24;
            this.lblTelephone.Text = "Téléphone";
            // 
            // txtPrenom
            // 
            this.txtPrenom.Location = new System.Drawing.Point(100, 45);
            this.txtPrenom.Name = "txtPrenom";
            this.txtPrenom.Size = new System.Drawing.Size(100, 20);
            this.txtPrenom.TabIndex = 17;
            // 
            // lblPrenom
            // 
            this.lblPrenom.AutoSize = true;
            this.lblPrenom.Location = new System.Drawing.Point(10, 48);
            this.lblPrenom.Name = "lblPrenom";
            this.lblPrenom.Size = new System.Drawing.Size(43, 13);
            this.lblPrenom.TabIndex = 16;
            this.lblPrenom.Text = "Prénom";
            // 
            // txtNom
            // 
            this.txtNom.Location = new System.Drawing.Point(100, 19);
            this.txtNom.Name = "txtNom";
            this.txtNom.Size = new System.Drawing.Size(100, 20);
            this.txtNom.TabIndex = 15;
            // 
            // lblNom
            // 
            this.lblNom.AutoSize = true;
            this.lblNom.Location = new System.Drawing.Point(10, 22);
            this.lblNom.Name = "lblNom";
            this.lblNom.Size = new System.Drawing.Size(29, 13);
            this.lblNom.TabIndex = 14;
            this.lblNom.Text = "Nom";
            // 
            // grpVehicule
            // 
            this.grpVehicule.Controls.Add(this.txtGamme);
            this.grpVehicule.Controls.Add(this.lblGamme);
            this.grpVehicule.Controls.Add(this.lblKms);
            this.grpVehicule.Controls.Add(this.numKms);
            this.grpVehicule.Controls.Add(this.txtImmat);
            this.grpVehicule.Controls.Add(this.lblImmat);
            this.grpVehicule.Controls.Add(this.txtSerie);
            this.grpVehicule.Controls.Add(this.lblSerie);
            this.grpVehicule.Enabled = false;
            this.grpVehicule.Location = new System.Drawing.Point(545, 12);
            this.grpVehicule.Name = "grpVehicule";
            this.grpVehicule.Size = new System.Drawing.Size(200, 130);
            this.grpVehicule.TabIndex = 30;
            this.grpVehicule.TabStop = false;
            this.grpVehicule.Text = "Vehicule";
            // 
            // txtGamme
            // 
            this.txtGamme.Location = new System.Drawing.Point(94, 97);
            this.txtGamme.Name = "txtGamme";
            this.txtGamme.Size = new System.Drawing.Size(100, 20);
            this.txtGamme.TabIndex = 42;
            // 
            // lblGamme
            // 
            this.lblGamme.AutoSize = true;
            this.lblGamme.Location = new System.Drawing.Point(6, 100);
            this.lblGamme.Name = "lblGamme";
            this.lblGamme.Size = new System.Drawing.Size(43, 13);
            this.lblGamme.TabIndex = 41;
            this.lblGamme.Text = "Gamme";
            // 
            // lblKms
            // 
            this.lblKms.AutoSize = true;
            this.lblKms.Location = new System.Drawing.Point(6, 73);
            this.lblKms.Name = "lblKms";
            this.lblKms.Size = new System.Drawing.Size(62, 13);
            this.lblKms.TabIndex = 39;
            this.lblKms.Text = "Kilométrage";
            // 
            // numKms
            // 
            this.numKms.Location = new System.Drawing.Point(94, 71);
            this.numKms.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.numKms.Name = "numKms";
            this.numKms.Size = new System.Drawing.Size(100, 20);
            this.numKms.TabIndex = 40;
            // 
            // txtImmat
            // 
            this.txtImmat.Location = new System.Drawing.Point(94, 45);
            this.txtImmat.Name = "txtImmat";
            this.txtImmat.Size = new System.Drawing.Size(100, 20);
            this.txtImmat.TabIndex = 34;
            // 
            // lblImmat
            // 
            this.lblImmat.AutoSize = true;
            this.lblImmat.Location = new System.Drawing.Point(6, 48);
            this.lblImmat.Name = "lblImmat";
            this.lblImmat.Size = new System.Drawing.Size(77, 13);
            this.lblImmat.TabIndex = 33;
            this.lblImmat.Text = "Immatriculation";
            // 
            // txtSerie
            // 
            this.txtSerie.Location = new System.Drawing.Point(94, 19);
            this.txtSerie.Name = "txtSerie";
            this.txtSerie.Size = new System.Drawing.Size(100, 20);
            this.txtSerie.TabIndex = 32;
            // 
            // lblSerie
            // 
            this.lblSerie.AutoSize = true;
            this.lblSerie.Location = new System.Drawing.Point(6, 22);
            this.lblSerie.Name = "lblSerie";
            this.lblSerie.Size = new System.Drawing.Size(74, 13);
            this.lblSerie.TabIndex = 31;
            this.lblSerie.Text = "Série / Fabric.";
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(671, 513);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(75, 23);
            this.btnExport.TabIndex = 47;
            this.btnExport.Text = "Export PDF";
            this.btnExport.UseVisualStyleBackColor = true;
            // 
            // gridMO
            // 
            this.gridMO.AllowUserToAddRows = false;
            this.gridMO.AllowUserToDeleteRows = false;
            this.gridMO.AllowUserToResizeColumns = false;
            this.gridMO.AllowUserToResizeRows = false;
            this.gridMO.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.gridMO.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridMO.Location = new System.Drawing.Point(12, 148);
            this.gridMO.Name = "gridMO";
            this.gridMO.ReadOnly = true;
            this.gridMO.RowHeadersVisible = false;
            this.gridMO.Size = new System.Drawing.Size(734, 104);
            this.gridMO.TabIndex = 48;
            // 
            // gridPiece
            // 
            this.gridPiece.AllowUserToAddRows = false;
            this.gridPiece.AllowUserToDeleteRows = false;
            this.gridPiece.AllowUserToResizeColumns = false;
            this.gridPiece.AllowUserToResizeRows = false;
            this.gridPiece.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.gridPiece.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridPiece.Location = new System.Drawing.Point(11, 284);
            this.gridPiece.Name = "gridPiece";
            this.gridPiece.ReadOnly = true;
            this.gridPiece.RowHeadersVisible = false;
            this.gridPiece.Size = new System.Drawing.Size(734, 104);
            this.gridPiece.TabIndex = 49;
            // 
            // lblTotalMo
            // 
            this.lblTotalMo.AutoSize = true;
            this.lblTotalMo.Location = new System.Drawing.Point(506, 261);
            this.lblTotalMo.Name = "lblTotalMo";
            this.lblTotalMo.Size = new System.Drawing.Size(103, 13);
            this.lblTotalMo.TabIndex = 50;
            this.lblTotalMo.Text = "Total Main d\'Oeuvre";
            // 
            // numTotalMo
            // 
            this.numTotalMo.DecimalPlaces = 2;
            this.numTotalMo.Enabled = false;
            this.numTotalMo.Location = new System.Drawing.Point(615, 258);
            this.numTotalMo.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.numTotalMo.Name = "numTotalMo";
            this.numTotalMo.Size = new System.Drawing.Size(100, 20);
            this.numTotalMo.TabIndex = 51;
            // 
            // lblTotalMoEuros
            // 
            this.lblTotalMoEuros.AutoSize = true;
            this.lblTotalMoEuros.Location = new System.Drawing.Point(721, 261);
            this.lblTotalMoEuros.Name = "lblTotalMoEuros";
            this.lblTotalMoEuros.Size = new System.Drawing.Size(13, 13);
            this.lblTotalMoEuros.TabIndex = 52;
            this.lblTotalMoEuros.Text = "€";
            // 
            // lblTotalPieceEuros
            // 
            this.lblTotalPieceEuros.AutoSize = true;
            this.lblTotalPieceEuros.Location = new System.Drawing.Point(721, 397);
            this.lblTotalPieceEuros.Name = "lblTotalPieceEuros";
            this.lblTotalPieceEuros.Size = new System.Drawing.Size(13, 13);
            this.lblTotalPieceEuros.TabIndex = 55;
            this.lblTotalPieceEuros.Text = "€";
            // 
            // numTotalPiece
            // 
            this.numTotalPiece.DecimalPlaces = 2;
            this.numTotalPiece.Enabled = false;
            this.numTotalPiece.Location = new System.Drawing.Point(615, 394);
            this.numTotalPiece.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.numTotalPiece.Name = "numTotalPiece";
            this.numTotalPiece.Size = new System.Drawing.Size(100, 20);
            this.numTotalPiece.TabIndex = 54;
            // 
            // lblTotalPiece
            // 
            this.lblTotalPiece.AutoSize = true;
            this.lblTotalPiece.Location = new System.Drawing.Point(543, 397);
            this.lblTotalPiece.Name = "lblTotalPiece";
            this.lblTotalPiece.Size = new System.Drawing.Size(66, 13);
            this.lblTotalPiece.TabIndex = 53;
            this.lblTotalPiece.Text = "Total Pièces";
            // 
            // gridMontantTva
            // 
            this.gridMontantTva.AllowUserToAddRows = false;
            this.gridMontantTva.AllowUserToDeleteRows = false;
            this.gridMontantTva.AllowUserToResizeColumns = false;
            this.gridMontantTva.AllowUserToResizeRows = false;
            this.gridMontantTva.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.gridMontantTva.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridMontantTva.Location = new System.Drawing.Point(12, 420);
            this.gridMontantTva.Name = "gridMontantTva";
            this.gridMontantTva.ReadOnly = true;
            this.gridMontantTva.RowHeadersVisible = false;
            this.gridMontantTva.Size = new System.Drawing.Size(364, 104);
            this.gridMontantTva.TabIndex = 56;
            // 
            // gridTotalTva
            // 
            this.gridTotalTva.AllowUserToAddRows = false;
            this.gridTotalTva.AllowUserToDeleteRows = false;
            this.gridTotalTva.AllowUserToResizeColumns = false;
            this.gridTotalTva.AllowUserToResizeRows = false;
            this.gridTotalTva.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.gridTotalTva.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridTotalTva.Location = new System.Drawing.Point(382, 420);
            this.gridTotalTva.Name = "gridTotalTva";
            this.gridTotalTva.ReadOnly = true;
            this.gridTotalTva.RowHeadersVisible = false;
            this.gridTotalTva.Size = new System.Drawing.Size(364, 61);
            this.gridTotalTva.TabIndex = 57;
            // 
            // lblTotalAPayerEuros
            // 
            this.lblTotalAPayerEuros.AutoSize = true;
            this.lblTotalAPayerEuros.Location = new System.Drawing.Point(721, 490);
            this.lblTotalAPayerEuros.Name = "lblTotalAPayerEuros";
            this.lblTotalAPayerEuros.Size = new System.Drawing.Size(13, 13);
            this.lblTotalAPayerEuros.TabIndex = 60;
            this.lblTotalAPayerEuros.Text = "€";
            // 
            // numTotalAPayer
            // 
            this.numTotalAPayer.DecimalPlaces = 2;
            this.numTotalAPayer.Enabled = false;
            this.numTotalAPayer.Location = new System.Drawing.Point(615, 487);
            this.numTotalAPayer.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.numTotalAPayer.Name = "numTotalAPayer";
            this.numTotalAPayer.Size = new System.Drawing.Size(100, 20);
            this.numTotalAPayer.TabIndex = 59;
            // 
            // lblTotalAPayer
            // 
            this.lblTotalAPayer.AutoSize = true;
            this.lblTotalAPayer.Location = new System.Drawing.Point(543, 490);
            this.lblTotalAPayer.Name = "lblTotalAPayer";
            this.lblTotalAPayer.Size = new System.Drawing.Size(69, 13);
            this.lblTotalAPayer.TabIndex = 58;
            this.lblTotalAPayer.Text = "Total à payer";
            // 
            // FrmFacture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(758, 543);
            this.Controls.Add(this.lblTotalAPayerEuros);
            this.Controls.Add(this.numTotalAPayer);
            this.Controls.Add(this.lblTotalAPayer);
            this.Controls.Add(this.gridTotalTva);
            this.Controls.Add(this.gridMontantTva);
            this.Controls.Add(this.lblTotalPieceEuros);
            this.Controls.Add(this.numTotalPiece);
            this.Controls.Add(this.lblTotalPiece);
            this.Controls.Add(this.lblTotalMoEuros);
            this.Controls.Add(this.numTotalMo);
            this.Controls.Add(this.lblTotalMo);
            this.Controls.Add(this.gridPiece);
            this.Controls.Add(this.gridMO);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.grpVehicule);
            this.Controls.Add(this.grpClient);
            this.Controls.Add(this.grpFacture);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmFacture";
            this.Text = "Détails de Facture";
            this.Load += new System.EventHandler(this.FrmFacture_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numOrdre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numActivite)).EndInit();
            this.grpFacture.ResumeLayout(false);
            this.grpFacture.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numId)).EndInit();
            this.grpClient.ResumeLayout(false);
            this.grpClient.PerformLayout();
            this.grpVehicule.ResumeLayout(false);
            this.grpVehicule.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numKms)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPiece)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTotalMo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTotalPiece)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMontantTva)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTotalTva)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTotalAPayer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDateFacture;
        private System.Windows.Forms.DateTimePicker dtpDateFacture;
        private System.Windows.Forms.Label lblNumOrdre;
        private System.Windows.Forms.NumericUpDown numOrdre;
        private System.Windows.Forms.NumericUpDown numActivite;
        private System.Windows.Forms.Label lblNumAct;
        private System.Windows.Forms.GroupBox grpFacture;
        private System.Windows.Forms.GroupBox grpClient;
        private System.Windows.Forms.TextBox txtMobile;
        private System.Windows.Forms.Label lblMobile;
        private System.Windows.Forms.TextBox txtTelephone;
        private System.Windows.Forms.Label lblTelephone;
        private System.Windows.Forms.TextBox txtPrenom;
        private System.Windows.Forms.Label lblPrenom;
        private System.Windows.Forms.TextBox txtNom;
        private System.Windows.Forms.Label lblNom;
        private System.Windows.Forms.GroupBox grpVehicule;
        private System.Windows.Forms.TextBox txtSerie;
        private System.Windows.Forms.Label lblSerie;
        private System.Windows.Forms.TextBox txtImmat;
        private System.Windows.Forms.Label lblImmat;
        private System.Windows.Forms.TextBox txtGamme;
        private System.Windows.Forms.Label lblGamme;
        private System.Windows.Forms.Label lblKms;
        private System.Windows.Forms.NumericUpDown numKms;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.NumericUpDown numId;
        private System.Windows.Forms.DataGridView gridMO;
        private System.Windows.Forms.DataGridView gridPiece;
        private System.Windows.Forms.Label lblTotalMo;
        private System.Windows.Forms.NumericUpDown numTotalMo;
        private System.Windows.Forms.Label lblTotalMoEuros;
        private System.Windows.Forms.Label lblTotalPieceEuros;
        private System.Windows.Forms.NumericUpDown numTotalPiece;
        private System.Windows.Forms.Label lblTotalPiece;
        private System.Windows.Forms.DataGridView gridMontantTva;
        private System.Windows.Forms.DataGridView gridTotalTva;
        private System.Windows.Forms.Label lblTotalAPayerEuros;
        private System.Windows.Forms.NumericUpDown numTotalAPayer;
        private System.Windows.Forms.Label lblTotalAPayer;
    }
}