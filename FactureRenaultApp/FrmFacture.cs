﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FactureRenaultApp.classes;

namespace FactureRenaultApp
{
    public partial class FrmFacture : Form
    {
        public Facture Facture { get; set; }
        public FrmFacture()
        {
            InitializeComponent();
        }

        private void FrmFacture_Load(object sender, EventArgs e)
        {
            // Groupe Facture
            numId.Value = Facture.Id;
            dtpDateFacture.Value = Facture.DateFacturation;
            numOrdre.Value = Facture.NumeroOrdre;
            numActivite.Value = Facture.NumeroActivité;

            // Groupe Véhicule
            Vehicule vehicule = Facture.Vehicule;

            txtSerie.Text = vehicule.NumeroSérie;
            txtImmat.Text = vehicule.Immatriculation;
            numKms.Value = vehicule.Kilométrage;
            txtGamme.Text = vehicule.Gamme;

            // Groupe Client
            Client client = vehicule.Client;

            txtNom.Text = client.Nom;
            txtPrenom.Text = client.Prénom;
            txtTelephone.Text = client.Telephone;
            txtMobile.Text = client.Mobile;

            // Data Grid View Main d'Oeuvre
            gridMO.DataSource = LigneFacture.GetLignesFactures(Facture.Id, TypeElement.MO);
            gridMO.ClearSelection();

            // Data Grid View Pièces
            gridPiece.DataSource = LigneFacture.GetLignesFactures(Facture.Id, TypeElement.Piece);
            gridPiece.ClearSelection();

            // Data Grid View Montant TVA
            gridMontantTva.DataSource = LigneMontantTVA.GetLignesMontantsTva(Facture.Id);
            gridMontantTva.ClearSelection();

            // Data Grid View Total TVA
            gridTotalTva.DataSource = LigneTotalTva.GetLignesTotalTva(Facture.Id);
            gridTotalTva.ClearSelection();

            // Total MO
            numTotalMo.Value = Facture.GetTotalMo(Facture.Id);

            // Total Pièces
            numTotalPiece.Value = Facture.GetTotalPiece(Facture.Id);

            // Total MO
            numTotalAPayer.Value = Facture.GetTotalTtc(Facture.Id);
        }
    }
}
