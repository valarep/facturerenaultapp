﻿namespace FactureRenaultApp
{
    partial class FrmListFactures
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lstFactures = new System.Windows.Forms.ListBox();
            this.bsFactures = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.bsFactures)).BeginInit();
            this.SuspendLayout();
            // 
            // lstFactures
            // 
            this.lstFactures.FormattingEnabled = true;
            this.lstFactures.Location = new System.Drawing.Point(12, 12);
            this.lstFactures.Name = "lstFactures";
            this.lstFactures.Size = new System.Drawing.Size(194, 420);
            this.lstFactures.TabIndex = 0;
            this.lstFactures.DoubleClick += new System.EventHandler(this.lstFactures_DoubleClick);
            // 
            // FrmListFactures
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lstFactures);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmListFactures";
            this.Text = "Factures";
            this.Load += new System.EventHandler(this.FrmFacture_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bsFactures)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lstFactures;
        private System.Windows.Forms.BindingSource bsFactures;
    }
}

