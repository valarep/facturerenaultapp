﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FactureRenaultApp.classes;

namespace FactureRenaultApp
{
    public partial class FrmListFactures : Form
    {
        List<Facture> factures;
        public FrmListFactures()
        {
            InitializeComponent();
        }

        private void FrmFacture_Load(object sender, EventArgs e)
        {
            factures = Facture.GetFactures();
            bsFactures.DataSource = factures;
            lstFactures.DataSource = bsFactures;
            lstFactures.DisplayMember = "Id";
        }

        private void lstFactures_DoubleClick(object sender, EventArgs e)
        {
            Facture facture = (Facture)lstFactures.SelectedItem;
            if (facture != null)
            {
                FrmFacture frmFacture = new FrmFacture();
                frmFacture.Facture = facture;
                frmFacture.ShowDialog();
            }
        }
    }
}
