﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactureRenaultApp.classes
{
    public class Element
    {
        public int Id { get; set; }
        public string Reference { get; set; }
        public string Designation { get; set; }
        public decimal PrixHTActuel { get; set; }
        public TypeElement Type { get; set; }
        public int IdTVA { get; set; }
        public TVA TVA { get; set; }
    }
}
