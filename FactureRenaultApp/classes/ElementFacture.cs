﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactureRenaultApp.classes
{
    public class ElementFacture
    {
        public int IdFacture { get; set; }
        public int IdElement { get; set; }
        public decimal Quantité { get; set; }
        public decimal PrixHTFacture { get; set; }
        public decimal Remise { get; set; }
    }
}
