﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactureRenaultApp.dao;

namespace FactureRenaultApp.classes
{
    public class Facture
    {
        public int Id { get; set; }
        public DateTime DateFacturation { get; set; }
        public int NumeroOrdre { get; set; }
        public int NumeroActivité { get; set; }
        public string Accueil { get; set; }
        public DateTime DatePaiement { get; set; }
        public int IdVehicule { get; set; }
        private Vehicule vehicule;
        public Vehicule Vehicule { 
            get 
            {
                if (vehicule == null)
                {
                    vehicule = Vehicule.GetVehicule(IdVehicule);
                }
                return vehicule;
            }
            set
            {
                vehicule = value;
                IdVehicule = value.Id;
            }
        }
        public Dictionary<Element, ElementFacture> Elements { get; set; }
        public static List<Facture> GetFactures() => FactureDao.GetFactures();
        public static decimal GetTotalMo(int id_facture) => FactureDao.GetTotalMo(id_facture);
        public static decimal GetTotalPiece(int id_facture) => FactureDao.GetTotalPiece(id_facture);
        public static decimal GetTotalTtc(int id_facture) => FactureDao.GetTotalTtc(id_facture);
    }
}
