﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactureRenaultApp.dao;

namespace FactureRenaultApp.classes
{
    public class LigneFacture
    {
        public string Designation { get; set; }
        public string Reference { get; set; }
        public decimal Quantite { get; set; }
        public decimal PrixHt { get; set; }
        public decimal Remise { get; set; }
        public decimal PrixHtNet { get; set; }
        public decimal MontantHt { get; set; }
        public int IdTva { get; set; }

        public static List<LigneFacture> GetLignesFactures(int id_facture, TypeElement type) => LigneFactureDao.GetLignesFactures(id_facture, type);
    }
}
