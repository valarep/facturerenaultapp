﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactureRenaultApp.dao;

namespace FactureRenaultApp.classes
{
    public class LigneMontantTVA
    {
        public int IdTva { get; set; }
        public decimal Taux { get; set; }
        public decimal MontantHt { get; set; }
        public decimal MontantTva { get; set; }
        public decimal MontantTtc { get; set; }

        public static List<LigneMontantTVA> GetLignesMontantsTva(int id_facture) => LigneMontantTvaDao.GetLignesMontantsTva(id_facture);
    }
}
