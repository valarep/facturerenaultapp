﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactureRenaultApp.dao;

namespace FactureRenaultApp.classes
{
    public class LigneTotalTva
    {
        public decimal TotalHt { get; set; }
        public decimal TotalTva { get; set; }
        public decimal TotalTtc { get; set; }

        public static List<LigneTotalTva> GetLignesTotalTva(int id_facture) => LigneTotalTvaDao.GetLignesTotalTva(id_facture);
    }
}
