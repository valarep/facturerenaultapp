﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactureRenaultApp.dao;

namespace FactureRenaultApp.classes
{
    public class Vehicule
    {
        public int Id { get; set; }
        public string NumeroSérie { get; set; }
        public string Immatriculation { get; set; }
        public DateTime MiseEnCirculation { get; set; }
        public DateTime Livraison { get; set; }
        public int Kilométrage { get; set; }
        public string Gamme { get; set; }
        public string Modèle { get; set; }
        public string TypeVarianteVersion { get; set; }
        public int IdClient { get; set; }
        private Client client;
        public Client Client 
        {
            get 
            {
                if(client == null)
                {
                    client = Client.GetClient(IdClient);
                }
                return client;
            }
        }
        public static Vehicule GetVehicule(int id) => VehiculeDao.GetVehicule(id);
    }
}
