﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using FactureRenaultApp.classes;

namespace FactureRenaultApp.dao
{
    public static class ClientDao
    {
        public static Client GetClient(int id)
        {
            Client item = null;
            using (MySqlConnection connection = new MySqlConnection(Dao.ConnectionString))
            {
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM `client` WHERE `id` = ?id;";
                cmd.Parameters.Add(new MySqlParameter("id", id));
                connection.Open();
                MySqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    item = new Client
                    {
                        Id = (int)reader["id"],
                        Nom = (string)reader["nom"],
                        Prénom = (string)reader["prenom"],
                        Adresse = (string)reader["adresse"],
                        CodePostal = (string)reader["cp"],
                        Ville = (string)reader["ville"],
                        Telephone = (string)reader["tel"],
                        Mobile = (string)reader["mobile"],
                        Compte = (string)reader["num_compte"],
                    };
                }
            }

            return item;
        }
    }
}
