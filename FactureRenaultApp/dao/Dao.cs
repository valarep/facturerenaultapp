﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace FactureRenaultApp.dao
{
    public static class Dao
    {
        private static string _connectionString;
        public static string ConnectionString
        {
            get
            {
                if (_connectionString == null)
                {
                    MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder();

                    builder.Server = ConfigurationManager.AppSettings.Get("Server");
                    builder.Port = Convert.ToUInt32(ConfigurationManager.AppSettings.Get("Port"));
                    builder.UserID = ConfigurationManager.AppSettings.Get("UserID");
                    builder.Password = ConfigurationManager.AppSettings.Get("Password");
                    builder.Database = ConfigurationManager.AppSettings.Get("Database");
                    builder.CharacterSet = ConfigurationManager.AppSettings.Get("CharacterSet");

                    _connectionString = builder.ToString();
                }

                return _connectionString;
            }
        }
    }
}
