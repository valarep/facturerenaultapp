﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using FactureRenaultApp.classes;

namespace FactureRenaultApp.dao
{
    public static class FactureDao
    {
        public static List<Facture> GetFactures()
        {
            List<Facture> items = new List<Facture>();
            using (MySqlConnection connection = new MySqlConnection(Dao.ConnectionString))
            {
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM `facture`;";
                connection.Open();
                MySqlDataReader reader = cmd.ExecuteReader();
                while(reader.Read())
                {
                    Facture item = new Facture {
                        Id = (int)reader["id"],
                        DateFacturation = (DateTime)reader["date_fact"],
                        NumeroOrdre = (int)reader["num_or"],
                        NumeroActivité = (int)reader["num_act"],
                        Accueil = (string)reader["accueil"],
                        DatePaiement = (DateTime)reader["date_paie"],
                        IdVehicule = (int)reader["id_vehicule"],
                    };
                    items.Add(item);
                }
            }
            return items;
        }

        public static decimal GetTotalMo(int id_facture)
        {
            decimal result;
            using (MySqlConnection connection = new MySqlConnection(Dao.ConnectionString))
            {
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT `total_mo`(?id_facture) as total;";
                cmd.Parameters.Add(new MySqlParameter("id_facture", id_facture));
                connection.Open();
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                result = (decimal)reader["total"];
            }
            return result;
        }
        public static decimal GetTotalPiece(int id_facture)
        {
            decimal result;
            using (MySqlConnection connection = new MySqlConnection(Dao.ConnectionString))
            {
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT `total_piece`(?id_facture) as total;";
                cmd.Parameters.Add(new MySqlParameter("id_facture", id_facture));
                connection.Open();
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                result = (decimal)reader["total"];
            }
            return result;
        }
        public static decimal GetTotalTtc(int id_facture)
        {
            decimal result;
            using (MySqlConnection connection = new MySqlConnection(Dao.ConnectionString))
            {
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT `total_ttc`(?id_facture) as total;";
                cmd.Parameters.Add(new MySqlParameter("id_facture", id_facture));
                connection.Open();
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                result = (decimal)reader["total"];
            }
            return result;
        }
    }
}
