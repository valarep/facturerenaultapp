﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using FactureRenaultApp.classes;

namespace FactureRenaultApp.dao
{
    public static class LigneFactureDao
    {
        public static List<LigneFacture> GetLignesFactures(int id_facture, TypeElement type)
        {
            List<LigneFacture> items = new List<LigneFacture>();
            using (MySqlConnection connection = new MySqlConnection(Dao.ConnectionString))
            {
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "CALL `proc_lignes_facture`(?id_facture, ?type);";
                cmd.Parameters.Add(new MySqlParameter("id_facture", id_facture));
                cmd.Parameters.Add(new MySqlParameter("type", type.ToString()));
                connection.Open();
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    LigneFacture item = new LigneFacture
                    {
                        Designation = (string)reader["designation"],
                        Reference = (string)reader["reference"],
                        Quantite = (decimal)reader["quantite"],
                        PrixHt = (decimal)reader["prix_ht"],
                        Remise = (decimal)reader["remise"],
                        PrixHtNet = (decimal)reader["prix_ht_net"],
                        MontantHt = (decimal)reader["montant_ht"],
                        IdTva = (int)reader["id_tva"],
                    };
                    items.Add(item);
                }
            }
            return items;
        }
    }
}
