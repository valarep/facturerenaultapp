﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using FactureRenaultApp.classes;

namespace FactureRenaultApp.dao
{
    public static class LigneMontantTvaDao
    {
        public static List<LigneMontantTVA> GetLignesMontantsTva(int id_facture)
        {
            List<LigneMontantTVA> items = new List<LigneMontantTVA>();
            using (MySqlConnection connection = new MySqlConnection(Dao.ConnectionString))
            {
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "CALL `proc_montant_tva`(?id_facture);";
                cmd.Parameters.Add(new MySqlParameter("id_facture", id_facture));
                connection.Open();
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    LigneMontantTVA item = new LigneMontantTVA
                    {
                        IdTva = (int)reader["code"],
                        Taux = (decimal)reader["taux"],
                        MontantHt = (decimal)reader["montant_ht"],
                        MontantTva = (decimal)reader["montant_tva"],
                        MontantTtc = (decimal)reader["montant_ttc"],
                    };
                    items.Add(item);
                }
            }
            return items;
        }
    }
}
