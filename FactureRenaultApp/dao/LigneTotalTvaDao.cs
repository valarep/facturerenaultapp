﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using FactureRenaultApp.classes;

namespace FactureRenaultApp.dao
{
    public static class LigneTotalTvaDao
    {
        public static List<LigneTotalTva> GetLignesTotalTva(int id_facture)
        {
            List<LigneTotalTva> items = new List<LigneTotalTva>();
            using (MySqlConnection connection = new MySqlConnection(Dao.ConnectionString))
            {
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "CALL `proc_total_tva`(?id_facture);";
                cmd.Parameters.Add(new MySqlParameter("id_facture", id_facture));
                connection.Open();
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    LigneTotalTva item = new LigneTotalTva
                    {
                        TotalHt = (decimal)reader["total_ht"],
                        TotalTva = (decimal)reader["total_tva"],
                        TotalTtc = (decimal)reader["total_ttc"],
                    };
                    items.Add(item);
                }
            }
            return items;
        }
    }
}
