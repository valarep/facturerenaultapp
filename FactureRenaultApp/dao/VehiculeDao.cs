﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using FactureRenaultApp.classes;

namespace FactureRenaultApp.dao
{
    public class VehiculeDao
    {
        public static Vehicule GetVehicule(int id)
        {
            Vehicule item = null;
            using (MySqlConnection connection = new MySqlConnection(Dao.ConnectionString))
            {
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM `vehicule` WHERE `id` = ?id;";
                cmd.Parameters.Add(new MySqlParameter("id", id));
                connection.Open();
                MySqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    item = new Vehicule
                    {
                        Id = (int)reader["id"],
                        NumeroSérie = (string)reader["num_serie"],
                        Immatriculation = (string)reader["immat"],
                        MiseEnCirculation = (DateTime)reader["mec"],
                        Livraison = (DateTime)reader["livraison"],
                        Kilométrage = (int)reader["kms"],
                        Gamme = (string)reader["gamme"],
                        Modèle = (string)reader["modele"],
                        TypeVarianteVersion = (string)reader["tvv"],
                        IdClient = (int)reader["id_client"],
                    };
                }
            }

            return item;
        }
    }
}
