-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 24 jan. 2020 à 23:29
-- Version du serveur :  5.7.21
-- Version de PHP :  7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `facturerenaultapp`
--

DELIMITER $$
--
-- Procédures
--
DROP PROCEDURE IF EXISTS `proc_lignes_facture`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_lignes_facture` (IN `p_id_facture` INT, IN `p_type` ENUM('mo','piece'))  NO SQL
BEGIN
	SELECT element.designation as designation,
           element.reference as reference,
           element_facture.quantite as quantite,
           element_facture.prix_ht_facture as prix_ht,
           element_facture.remise as remise,
           ROUND(element_facture.prix_ht_facture * (100 - element_facture.remise) / 100, 2) as prix_ht_net,
           ROUND(element_facture.quantite * element_facture.prix_ht_facture * (100 - element_facture.remise) / 100, 2) as montant_ht,
           element.id_tva as id_tva
	FROM element_facture
    INNER JOIN element ON element_facture.id_element = element.id
    WHERE element_facture.id_facture = p_id_facture
    AND element.type = p_type;
END$$

DROP PROCEDURE IF EXISTS `proc_montant_tva`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_montant_tva` (IN `p_id_facture` INT)  NO SQL
BEGIN
SELECT
	tva.id as code,
    tva.taux as taux,
	montant_ht(p_id_facture, tva.id) as montant_ht, 
    montant_tva(p_id_facture, tva.id) as montant_tva, 
    montant_ttc(p_id_facture, tva.id) as montant_ttc 
FROM tva 
INNER JOIN element ON element.id_tva = tva.id 
INNER JOIN element_facture ON element_facture.id_element = element.id 
WHERE element_facture.id_facture = p_id_facture 
GROUP BY tva.id;
END$$

DROP PROCEDURE IF EXISTS `proc_total_piece_mo`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_total_piece_mo` (IN `p_id_facture` INT)  NO SQL
BEGIN
	SELECT total_piece(p_id_facture) as total_piece,
           total_mo(p_id_facture) as total_mo;
END$$

DROP PROCEDURE IF EXISTS `proc_total_tva`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_total_tva` (IN `p_id_facture` INT)  NO SQL
BEGIN
SELECT
	total_ht(p_id_facture) as total_ht, 
    total_tva(p_id_facture) as total_tva, 
    total_ttc(p_id_facture) as total_ttc;
END$$

--
-- Fonctions
--
DROP FUNCTION IF EXISTS `montant_ht`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `montant_ht` (`p_id_facture` INT, `p_id_tva` INT) RETURNS DECIMAL(7,2) NO SQL
BEGIN
	DECLARE result DECIMAL(7,2);
    
	SELECT SUM(quantite * prix_ht_facture * (100 - remise) / 100) INTO result
    FROM element_facture
    INNER JOIN element ON element_facture.id_element = element.id
    WHERE element_facture.id_facture = p_id_facture
    AND element.id_tva = p_id_tva;
    
    RETURN result;
END$$

DROP FUNCTION IF EXISTS `montant_ttc`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `montant_ttc` (`p_id_facture` INT, `p_id_tva` INT) RETURNS DECIMAL(7,2) NO SQL
BEGIN
	DECLARE result DECIMAL(7,2);
    
	SELECT SUM(quantite * prix_ht_facture * (100 - remise) / 100 * (100 + taux) / 100) INTO result
    FROM element_facture
    INNER JOIN element ON element_facture.id_element = element.id
    INNER JOIN tva ON element.id_tva = tva.id
    WHERE element_facture.id_facture = p_id_facture
    AND element.id_tva = p_id_tva;
    
    RETURN result;
END$$

DROP FUNCTION IF EXISTS `montant_tva`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `montant_tva` (`p_id_facture` INT, `p_id_tva` INT) RETURNS DECIMAL(7,2) NO SQL
BEGIN
	DECLARE result DECIMAL(7,2);
    
	SELECT SUM(quantite * prix_ht_facture * (100 - remise) / 100 * taux / 100) INTO result
    FROM element_facture
    INNER JOIN element ON element_facture.id_element = element.id
    INNER JOIN tva ON element.id_tva = tva.id
    WHERE element_facture.id_facture = p_id_facture
    AND element.id_tva = p_id_tva;
    
    RETURN result;
END$$

DROP FUNCTION IF EXISTS `total_ht`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `total_ht` (`p_id_facture` INT) RETURNS DECIMAL(7,2) NO SQL
BEGIN
	DECLARE result DECIMAL(7,2);
    
	SELECT SUM(quantite * prix_ht_facture * (100 - remise) / 100) INTO result
    FROM element_facture
    INNER JOIN element ON element_facture.id_element = element.id
    WHERE element_facture.id_facture = p_id_facture;
    
    RETURN result;
END$$

DROP FUNCTION IF EXISTS `total_mo`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `total_mo` (`p_id_facture` INT) RETURNS DECIMAL(7,2) NO SQL
BEGIN
	DECLARE result DECIMAL(7,2);

	SELECT total_type('mo', p_id_facture) INTO result;
    
    RETURN result;
END$$

DROP FUNCTION IF EXISTS `total_piece`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `total_piece` (`p_id_facture` INT) RETURNS DECIMAL(7,2) NO SQL
BEGIN
	DECLARE result DECIMAL(7,2);

	SELECT total_type('piece',p_id_facture) INTO result;
    
    RETURN result;
END$$

DROP FUNCTION IF EXISTS `total_ttc`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `total_ttc` (`p_id_facture` INT) RETURNS DECIMAL(7,2) NO SQL
BEGIN
	DECLARE result DECIMAL(7,2);
    
	SELECT SUM(quantite * prix_ht_facture * (100 - remise) / 100 * (100 + taux) / 100) INTO result
    FROM element_facture
    INNER JOIN element ON element_facture.id_element = element.id
    INNER JOIN tva ON element.id_tva = tva.id
    WHERE element_facture.id_facture = p_id_facture;
    
    RETURN result;
END$$

DROP FUNCTION IF EXISTS `total_tva`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `total_tva` (`p_id_facture` INT) RETURNS DECIMAL(7,2) NO SQL
BEGIN
	DECLARE result DECIMAL(7,2);
    
	SELECT SUM(quantite * prix_ht_facture * (100 - remise) / 100 * taux / 100) INTO result
    FROM element_facture
    INNER JOIN element ON element_facture.id_element = element.id
    INNER JOIN tva ON element.id_tva = tva.id
    WHERE element_facture.id_facture = p_id_facture;
    
    RETURN result;
END$$

DROP FUNCTION IF EXISTS `total_type`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `total_type` (`p_type` ENUM('mo','piece'), `p_id_facture` INT) RETURNS DECIMAL(7,2) NO SQL
BEGIN
	DECLARE result DECIMAL(7,2);

	SELECT SUM(quantite * prix_ht_facture * (100 - remise) / 100) INTO result
    FROM element_facture
    INNER JOIN element ON element_facture.id_element = element.id
    WHERE element.type = p_type
    AND element_facture.id_facture = p_id_facture;
    
    RETURN result;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `adresse` varchar(200) DEFAULT NULL,
  `cp` varchar(6) DEFAULT NULL,
  `ville` varchar(50) DEFAULT NULL,
  `tel` varchar(10) DEFAULT NULL,
  `mobile` varchar(10) DEFAULT NULL,
  `num_compte` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `num_compte` (`num_compte`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`id`, `nom`, `prenom`, `adresse`, `cp`, `ville`, `tel`, `mobile`, `num_compte`) VALUES
(1, 'RIEHL', 'David', '16 place de la république', '59300', 'VALENCIENNES', '0327000000', '0606060606', '1234567890');

-- --------------------------------------------------------

--
-- Structure de la table `element`
--

DROP TABLE IF EXISTS `element`;
CREATE TABLE IF NOT EXISTS `element` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reference` varchar(10) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `prix_ht_actuel` decimal(7,2) NOT NULL,
  `type` enum('mo','piece') NOT NULL,
  `id_tva` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `reference` (`reference`),
  KEY `id_tva` (`id_tva`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `element`
--

INSERT INTO `element` (`id`, `reference`, `designation`, `prix_ht_actuel`, `type`, `id_tva`) VALUES
(1, '0048', 'OG REVISION VEHICULE', '45.00', 'mo', 2),
(2, '6170', 'OG REMPL FILTRE HABITACLE', '45.00', 'mo', 2),
(3, '3123', 'PERMUTATION DES 4 PNEUS ETE/HIVER', '45.00', 'mo', 2),
(4, '152095084R', 'CARTOUCHE FILTRE HUILE', '14.33', 'piece', 2),
(5, 'VX500R', 'YACCO VX500R 10W40', '4.81', 'piece', 2),
(6, '8200641648', 'RONDELLE ETANCHEITE', '2.10', 'piece', 2),
(7, '272773277R', 'FILTRE HABITACLE (RP: 272778214R)', '22.92', 'piece', 2),
(8, '7711238969', 'LAVE-GLACE HIVER', '2.58', 'piece', 2),
(9, '7711771317', 'MICHELIN 195/55 R16 91H E B 1 68 ALPIN 5 XL', '122.00', 'piece', 2),
(10, 'DECH', 'PARTICIPATION RECYCLAGE DES DECHETS', '2.00', 'piece', 2);

-- --------------------------------------------------------

--
-- Structure de la table `element_facture`
--

DROP TABLE IF EXISTS `element_facture`;
CREATE TABLE IF NOT EXISTS `element_facture` (
  `id_element` int(11) NOT NULL,
  `id_facture` int(11) NOT NULL,
  `quantite` decimal(4,2) NOT NULL,
  `prix_ht_facture` decimal(7,2) NOT NULL,
  `remise` decimal(4,2) NOT NULL,
  `num_ligne` int(11) NOT NULL,
  PRIMARY KEY (`id_element`,`id_facture`),
  KEY `id_element` (`id_element`),
  KEY `id_facture` (`id_facture`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `element_facture`
--

INSERT INTO `element_facture` (`id_element`, `id_facture`, `quantite`, `prix_ht_facture`, `remise`, `num_ligne`) VALUES
(1, 354731, '1.30', '45.00', '0.00', 1),
(2, 354731, '0.20', '45.00', '0.00', 2),
(3, 354731, '1.00', '45.00', '0.00', 3),
(4, 354731, '1.00', '14.33', '0.00', 4),
(5, 354731, '1.00', '4.81', '0.00', 5),
(6, 354731, '1.00', '2.10', '0.00', 6),
(7, 354731, '1.00', '22.92', '0.00', 7),
(8, 354731, '1.00', '2.58', '0.00', 8),
(9, 354731, '4.00', '122.00', '25.00', 9),
(10, 354731, '1.00', '2.00', '0.00', 10);

-- --------------------------------------------------------

--
-- Structure de la table `facture`
--

DROP TABLE IF EXISTS `facture`;
CREATE TABLE IF NOT EXISTS `facture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_fact` date NOT NULL,
  `num_or` int(11) NOT NULL,
  `num_act` int(11) NOT NULL,
  `accueil` varchar(50) NOT NULL,
  `date_paie` date NOT NULL,
  `id_vehicule` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_vehicule` (`id_vehicule`)
) ENGINE=InnoDB AUTO_INCREMENT=354732 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `facture`
--

INSERT INTO `facture` (`id`, `date_fact`, `num_or`, `num_act`, `accueil`, `date_paie`, `id_vehicule`) VALUES
(354731, '2018-12-20', 113923, 1, 'A renseigner', '2018-12-20', 1);

-- --------------------------------------------------------

--
-- Structure de la table `tva`
--

DROP TABLE IF EXISTS `tva`;
CREATE TABLE IF NOT EXISTS `tva` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taux` decimal(4,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tva`
--

INSERT INTO `tva` (`id`, `taux`) VALUES
(1, '10.00'),
(2, '20.00'),
(3, '5.50'),
(4, '2.10');

-- --------------------------------------------------------

--
-- Structure de la table `vehicule`
--

DROP TABLE IF EXISTS `vehicule`;
CREATE TABLE IF NOT EXISTS `vehicule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num_serie` varchar(25) NOT NULL,
  `immat` varchar(8) NOT NULL,
  `mec` date NOT NULL,
  `livraison` date NOT NULL,
  `kms` int(11) NOT NULL,
  `gamme` varchar(50) NOT NULL,
  `modele` varchar(50) NOT NULL,
  `tvv` varchar(10) NOT NULL,
  `id_client` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `immat` (`immat`),
  KEY `id_client` (`id_client`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `vehicule`
--

INSERT INTO `vehicule` (`id`, `num_serie`, `immat`, `mec`, `livraison`, `kms`, `gamme`, `modele`, `tvv`, `id_client`) VALUES
(1, 'UU1JSDDYG59254488/1208183', 'ES568QM', '2017-12-11', '2017-12-29', 16024, 'LODGY', 'SRY7', 'JSAY', 1);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `element`
--
ALTER TABLE `element`
  ADD CONSTRAINT `element_ibfk_1` FOREIGN KEY (`id_tva`) REFERENCES `tva` (`id`);

--
-- Contraintes pour la table `element_facture`
--
ALTER TABLE `element_facture`
  ADD CONSTRAINT `element_facture_ibfk_1` FOREIGN KEY (`id_element`) REFERENCES `element` (`id`),
  ADD CONSTRAINT `element_facture_ibfk_2` FOREIGN KEY (`id_facture`) REFERENCES `facture` (`id`);

--
-- Contraintes pour la table `facture`
--
ALTER TABLE `facture`
  ADD CONSTRAINT `facture_ibfk_1` FOREIGN KEY (`id_vehicule`) REFERENCES `vehicule` (`id`);

--
-- Contraintes pour la table `vehicule`
--
ALTER TABLE `vehicule`
  ADD CONSTRAINT `vehicule_ibfk_1` FOREIGN KEY (`id_client`) REFERENCES `client` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
